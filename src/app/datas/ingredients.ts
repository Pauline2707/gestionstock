import {IngredientModel} from '../models/ingredient.model';

export const listIngredients: IngredientModel[] = [
  new IngredientModel('Carotte'),
  new IngredientModel('Tomate'),
  new IngredientModel('Concombre'),
  new IngredientModel('Banane'),
  new IngredientModel('Cerise'),
  new IngredientModel('Framboise')
];
