import {ProduitModel} from '../models/produit.model';
import {listIngredients} from './ingredients';

export const listProduits: ProduitModel[] = [
  new ProduitModel(
    'Tarte',
    'La Mie Caline',
    'la_mie_caline@gmail.fr',
    listIngredients,
    'Belle tarte',
    12,
    'Conservation 12 mois.',
    30
  ),

  new ProduitModel(
    'Cake',
    'Le Fournil',
    'le_fournil@gmail.fr',
    listIngredients,
    'Beau cake aux carottes',
    2,
    'Conservation dans un endroit sec.',
    4565
  ),

  new ProduitModel(
    'Gâteau',
    'Pétrisane',
    'la_petrisane@gmail.fr',
    listIngredients,
    'Bon gâteau aux cerises griottes.',
    40,
    'Conservation dans un endroit peu ensoleillé.',
    3534
  ),

  new ProduitModel(
    'Quiche',
    'Traiteur',
    'traiteur@gmail.fr',
    listIngredients,
    'Sublime quiche aux concombres.',
    1,
    'Conservation au réfrigérateur.',
    14345
  ),

  new ProduitModel(
    'Lasagnes',
    'Leclerc',
    'leclerc@gmail.fr',
    listIngredients,
    'Splendides lasagnes aux framboises.',
    34,
    'Conservation dans un endroit très frais.',
    31
  )
];
