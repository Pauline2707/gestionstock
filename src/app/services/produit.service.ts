import {Injectable} from '@angular/core';
import {ProduitModel} from '../models/produit.model';
import {listProduits} from '../datas/produits';
import {listIngredients} from '../datas/ingredients';

@Injectable({
  providedIn: 'root'
})

export class ProduitService {
  public produits: ProduitModel[] = listProduits;

  constructor() {
  }

  getAll(): ProduitModel[] {
    return this.produits;
  }

  add(produit: ProduitModel) {
    const newProduit = new ProduitModel(
      produit.Nom,
      produit.Fournisseur,
      'null',
      listIngredients,
      produit.Description,
      produit.Age,
      'null',
      0
    );

    this.produits.push(newProduit);
  }

  delete(produit: ProduitModel) {
    this.produits = this.produits.filter(p => p.Nom !== produit.Nom);
  }
}
