import {Component, OnInit} from '@angular/core';
import {ProduitModel} from '../../models/produit.model';
import {ProduitService} from '../../services/produit.service';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-produits',
  templateUrl: './produits.component.html',
  styleUrls: ['./produits.component.css']
})
export class ProduitsComponent implements OnInit {
  public displayedColumns: String[] = ['Nom', 'Description', 'Ingrédients', 'Prix', 'Supprimer'];
  public dataSource = new BehaviorSubject([]);
  public showForm: boolean = false;

  constructor(private produitService: ProduitService) { }

  ngOnInit() {
    this.dataSource.next(this.produitService.getAll());
  }

  public produitIsAdd() {
    this.dataSource.next(this.produitService.getAll());
  }

  public deleteProduit(produit: ProduitModel) {
    this.produitService.delete(produit);
    // Recharge la liste
    this.dataSource.next(this.produitService.getAll());
  }

  public hideForm() {
    this.showForm = false;
  }
}
