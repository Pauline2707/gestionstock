import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ProduitService} from '../../../services/produit.service';
import {ProduitModel} from '../../../models/produit.model';

@Component({
  selector: 'app-form-produit',
  templateUrl: './form-produit.component.html',
  styleUrls: ['./form-produit.component.css']
})
export class FormProduitComponent implements OnInit {
  @Output() addEmit = new EventEmitter<any>();
  @Output() hideForm = new EventEmitter<any>();
  public nomInput: FormControl;
  public fournisseurInput: FormControl;
  public ageInput: FormControl;
  public descriptionInput: FormControl;
  public produitForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private produitService: ProduitService) {
    this.nomInput = formBuilder.control('', Validators.required);
    this.fournisseurInput = formBuilder.control('', Validators.required);
    this.ageInput = formBuilder.control('', Validators.required);
    this.descriptionInput = formBuilder.control('', Validators.required);
    this.produitForm = formBuilder.group({
        Nom: this.nomInput,
        Fournisseur: this.fournisseurInput,
        Age: this.ageInput,
        Description: this.descriptionInput
      }
    );
  }

  ngOnInit() {

  }

  public addProduit() {
    const produit: ProduitModel = this.produitForm.value;
    this.produitService.add(produit);
    this.addEmit.emit(produit);
    this.reset();
  }

  public reset() {
    this.nomInput.setValue('');
    this.fournisseurInput.setValue('');
    this.ageInput.setValue('');
    this.descriptionInput.setValue('');
    this.hideForm.emit();
  }
}
