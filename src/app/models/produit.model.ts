import {IngredientModel} from './ingredient.model';

export class ProduitModel {
  public Nom: String;
  public Fournisseur: String;
  public EmailFournisseur: String;
  public ListeIngredients: IngredientModel[];
  public Description: String;
  public Age: Number;
  public ConditionConservation: String;
  public Prix: Number;

  constructor(nom, fournisseur, email, ingredients, description, age, conservation, prix) {
    this.Nom = nom;
    this.Fournisseur = fournisseur;
    this.EmailFournisseur = email;
    this.ListeIngredients = ingredients;
    this.Description = description;
    this.Age = age;
    this.ConditionConservation = conservation;
    this.Prix = prix;
  }
}
